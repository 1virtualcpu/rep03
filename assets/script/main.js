window.addEventListener('load', () => {
  const mobileButton = document.querySelector('.mobile__button');

  mobileButton.addEventListener('click', () => {
    const mobileNav = document.querySelector('.mobile__nav');
    mobileNav.classList.toggle('show');
  }, false);
}, false);
